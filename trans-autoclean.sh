#!/bin/bash

BIN=transmission-remote

USER=trans-rpc-username
PASS=trans-rpc-password

$BIN -n $USER:$PASS -l | awk -v bin="$BIN" -v auth="$USER:$PASS" '$4 ~ /Done/ || $9 ~ /Finished/ { print bin" -n "auth" -t "$1" -r\0" }' | xargs -0 -I{} bash -c {}
