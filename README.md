trans-autoclean
===============

Automatically remove completed torrents from transmission using transmission-remote and cron

This script is incredibly simple, just fill in your details and then install it into cron. The script will check for any torrents that match the filter and then automatically remove any matching torrents.

INSTALL
=======

1. Place the script somewhere on your server

        /var/transmission/scripts/trans-autoclean.sh

2. Edit the config within the script

        USER=trans-rpc-username
        PASS=trans-rpc-password

3. Setup a cron job

        sudo crontab -e
        0 0 * * * /var/transmission/scripts/trans-autoclean.sh 2>&1 > /dev/null # run once a day

That's it, now your server will auto clean completed torrents (or any torrents that match your filter).

NOTE: no more "extra" functionality, it didn't actually work. feel free to edit the one-liner to meet extra needs, but this is probably sufficient for 99% of all users.